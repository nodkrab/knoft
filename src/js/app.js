try {
  window.$ = window.jQuery = require("jquery");
} catch (e) {}

import "../sass/app.sass";
import "jquery";
import "bootstrap";
import "sweetalert";
import "@fancyapps/fancybox";
import "jquery-ui";
import "slick-carousel-no-fonts";

// require("jquery-mousewheel")
require('malihu-custom-scrollbar-plugin')

$(window).on("load", function () {
  if ($(".holder").length > 0) {
    $(".holder").fadeOut("slow");
  }
});

var mobileHover = function () {
  "use strict";
  $("a")
    .on("touchstart", function () {
      $(this).trigger("hover");
    })
    .on("touchend", function () {
      $(this).trigger("hover");
    });
};

mobileHover();

//SVG
$("img.svg").each(function () {
  var $img = jQuery(this);
  var imgID = $img.attr("id");
  var imgClass = $img.attr("class");
  var imgURL = $img.attr("src");

  jQuery.get(
    imgURL,
    function (data) {
      // Get the SVG tag, ignore the rest
      var $svg = jQuery(data).find("svg");

      // Add replaced image's ID to the new SVG
      if (typeof imgID !== "undefined") {
        $svg = $svg.attr("id", imgID);
      }
      // Add replaced image's classes to the new SVG
      if (typeof imgClass !== "undefined") {
        $svg = $svg.attr("class", imgClass + " replaced-svg");
      }

      // Remove any invalid XML tags as per http://validator.w3.org
      $svg = $svg.removeAttr("xmlns:a");

      // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
      if (!$svg.attr("viewBox") && $svg.attr("height") && $svg.attr("width")) {
        $svg.attr(
          "viewBox",
          "0 0 " + $svg.attr("height") + " " + $svg.attr("width")
        );
      }

      // Replace image with new SVG
      $img.replaceWith($svg);
    },
    "xml"
  );
});

$("#sidebar li a[href^='#']").on('click', function (e) {
  // prevent default anchor click behavior
  e.preventDefault();
  // store hash
  var hash = this.hash;
  // animate
  $('html, body').animate({
    scrollTop: $(hash).offset().top
  }, 1000, function () {
    // when done, add hash to url
    // (default click behaviour)
    window.location.hash = hash;
  });

});


document.addEventListener("change", function (event) {
  let element = event.target;
  if (element && element.matches(".form-element-field")) {
    element.classList[element.value ? "add" : "remove"]("-hasvalue");
  }
});


var slideWrapper = $(".main-slider"),
  iframes = slideWrapper.find('.embed-player'),
  lazyImages = slideWrapper.find('.slide-image'),
  lazyCounter = 0;

// POST commands to YouTube or Vimeo API
function postMessageToPlayer(player, command) {
  if (player == null || command == null) return;
  player.contentWindow.postMessage(JSON.stringify(command), "*");
}

// When the slide is changing
function playPauseVideo(slick, control) {
  var currentSlide, slideType, startTime, player, video;

  currentSlide = slick.find(".slick-current");
  slideType = currentSlide.attr("class").split(" ")[1];
  player = currentSlide.find("iframe").get(0);
  startTime = currentSlide.data("video-start");

  if (slideType === "youtube") {
    switch (control) {
      case "play":
        postMessageToPlayer(player, {
          "event": "command",
          "func": "mute"
        });
        postMessageToPlayer(player, {
          "event": "command",
          "func": "playVideo"
        });
        break;
      case "pause":
        postMessageToPlayer(player, {
          "event": "command",
          "func": "pauseVideo"
        });
        break;
    }
  } else if (slideType === "video") {
    video = currentSlide.children("video").get(0);
    if (video != null) {
      if (control === "play") {
        video.play();
      } else {
        video.pause();
      }
    }
  }
}

// Resize player
function resizePlayer(iframes, ratio) {
  if (!iframes[0]) return;
  var win = $(".main-slider"),
    width = win.width(),
    playerWidth,
    height = win.height(),
    playerHeight,
    ratio = ratio || 16 / 9;

  iframes.each(function () {
    var current = $(this);
    if (width / ratio < height) {
      playerWidth = Math.ceil(height * ratio);
      current.width(playerWidth).height(height).css({
        left: (width - playerWidth) / 2,
        top: 0
      });
    } else {
      playerHeight = Math.ceil(width / ratio);
      current.width(width).height(playerHeight).css({
        left: 0,
        top: (height - playerHeight) / 2
      });
    }
  });
}

// DOM Ready
$(function () {
  // Initialize
  slideWrapper.on("init", function (slick) {
    slick = $(slick.currentTarget);
    setTimeout(function () {
      playPauseVideo(slick, "play");
    }, 1000);
    resizePlayer(iframes, 16 / 9);
  });
  slideWrapper.on("beforeChange", function (event, slick) {
    slick = $(slick.$slider);
    playPauseVideo(slick, "pause");
  });
  slideWrapper.on("afterChange", function (event, slick) {
    slick = $(slick.$slider);
    playPauseVideo(slick, "play");
  });
  slideWrapper.on("lazyLoaded", function (event, slick, image, imageSource) {
    lazyCounter++;
    if (lazyCounter === lazyImages.length) {
      lazyImages.addClass('show');
      // slideWrapper.slick("slickPlay");
    }
  });

  //start the slider
  slideWrapper.slick({
    //fade:true,
    autoplaySpeed: 4000,
    lazyLoad: "progressive",
    speed: 600,
    arrows: false,
    dots: true,
    cssEase: "cubic-bezier(0.87, 0.03, 0.41, 0.9)"
  });
});

// Resize event
$(window).on("resize.slickVideoPlayer", function () {
  resizePlayer(iframes, 16 / 9);
});